var express = require('express');
var router = express.Router();
var bicicletaControler = require('../../controllers/api/bicicletaControllerAPI');

router.get("/", bicicletaControler.bicicleta_list);
router.post("/create", bicicletaControler.bicicleta_create);
router.post("/delete", bicicletaControler.bicicleta_delete);
router.post("/update", bicicletaControler.bicicleta_update);

module.exports = router;